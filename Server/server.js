const express = require("express");
const connection = require("./config/db");
var cors = require('cors');
const app = express();
app.use(cors())

// Middleware
app.use(express.json({ extended: false }));

// Defile Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/survey', require('./routes/api/survey'));

const PORT = process.env.PORT || 5000;

// connection.query("SELECT * FROM patient", (err, result) => {
//     if (err) {
//         console.log(err)
//     }
//     console.log(result)
// })


app.get('/', (req, res) => {
    res.send('API RUNNING');
});

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
})