const express = require('express');
const router = express.Router();
const connection = require('../../config/db')
const auth = require('../../middleware/auth')


router.post('/create', (req, res) => {

    const { patient_id, survey_name, questions } = req.body;

    try {
        // In sert survey truoc -> lay id sau do luu vao question
        sql = "INSERT INTO survey (patient_id, survey_name) VALUES(?,?)"

        connection.query(sql, [patient_id, survey_name], (err, result) => {

            if (err) {
                console.log(err);
                return res.status(500).json('server error');
            }
            const survey_id = result.insertId;
            console.log(survey_id)

            var sql = "INSERT INTO question (survey_id, question_title) VALUES ?";
            let values = questions.map((question) => {
                return [survey_id, question];
            })

            connection.query(sql, [values], function (err, result) {
                if (err) throw err;
                res.send(result);
            });



        })


    } catch (err) {
        console.log(err.message);
        res.status(500).json({ msg: 'server err' });
    }
})

// Get answer survey
// GET
router.get('/answer/:survey_id/:result', auth, (req, res) => {
    const survey_id = req.params.survey_id;
    const user_id = req.user.id;
    const result = req.params.result;

    console.log(survey_id)
    console.log(user_id)
    console.log(result)

    try {
        // Insert answer_surver
        let sql = "INSERT INTO answer_survey (survey_id,user_id, result) VALUES(?,?,?)"
        connection.query(sql, [survey_id, user_id, result], (err, result) => {

            if (err) {
                console.log(err);
                return res.status(500).json('server error');
            }

            res.json(result)

        })


    } catch (err) {
        console.log(err.message);
        res.status(500).json('server error');
    }

});


// @ GET Survey ID

router.get('/answer/:survey_id', (req, res) => {
    const survey_id = req.params.survey_id;
    let sql = "SELECT * FROM question WHERE survey_id=?";
    connection.query(sql, [survey_id], (err, result) => {

        if (err) {
            console.log(err);
            return res.status(500).json('server error');
        }
        res.json(result)
    })
});


// GET ALL SURVEY
router.get('/list', (req, res) => {

    let sql = "SELECT *, count(user_id) as student_maked from survey, answer_survey where survey.survey_id=answer_survey.survey_id GROUP BY answer_survey.survey_id";
    connection.query(sql, (err, surveys) => {
        if (err) {
            console.log(err);
            return res.status(500).json('server error');
        }
        sql = "SELECT count(user_id) as students  from user where user_type=0";
        connection.query(sql, (err, studentCount) => {
            res.json({
                surveys,
                studentCount: studentCount[0].students 
        });
        });

    })
})

module.exports = router;