const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth')
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const connection = require('../../config/db')


// @route   POST api/auth
// @desc    Login
// @access  PUBLIC

router.post('/', [
    check('user_id', 'Please enter a user id').not().isEmpty(),
    check('user_password', 'Please enter a password').exists()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(404).json({ errors: errors.array() })
    }

    const { user_id, user_password } = req.body;

    try {
        // check user
        let sql = "SELECT * from user where user_id=?"
        connection.query(sql, [user_id], async (err, result) => {
            if (err) {
                console.log(err);
                return;
            }

            if (result.length == 0) {
                return res.status(400).json({ errors: [{ msg: 'Invalid Credential' }] });
            }

            const user = result[0];

            // check password
            const isSame = await bcrypt.compare(user_password, user.USER_PASSWORD);

            if (!isSame) {
                return res.status(400).json({ errors: [{ msg: 'Invalid Credential' }] });
            }

            // Return jsonwebtoken
            const payload = {
                user: {
                    id: user.USER_ID
                }
            }

            jwt.sign(payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    console.log(token);
                    res.json({ token });
                }
            )
        });

    } catch (error) {
        console.log(error.message);
        return res.status(500).send('Server Error');
    }

});

module.exports = router;