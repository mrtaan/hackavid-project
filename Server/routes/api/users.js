const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const connection = require('../../config/db')
const { check, validationResult } = require('express-validator')

router.get('/', (req, res) => {
    res.send('Hello')
})

// @route   POST api/users/register
// @desc    Register
// @access  PUBLIC
router.post('/register', [
    check('name', 'Name is required').not().isEmpty(),
    check('user_id', 'ID is required').not().isEmpty(),
    check('password', 'Please enter a password with 6 or more characters').isLength({ min: 6 })
], async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(404).json({ errors: errors.array() })
    }
    const { name, user_id, password } = req.body;

    try {
        let sql = "SELECT user_id from user where user_id=?"
        let user = connection.query(sql, [user_id], (err, result) => {
            if (err) {
                console.log(err);
                return;
            }

            if (result.length > 0) {
                return res.status(400).json({ errors: [{ msg: 'User already exists' }] });
            }

        });

        // Encrypt password
        const salt = await bcrypt.genSalt(10);
        const passwordHash = await bcrypt.hash(password, salt);

        sql = "INSERT INTO user (user_id, user_name, user_password) VALUES(?,?,?)"
        user = connection.query(sql, [user_id, name, passwordHash], (err, result) => {
            if (err) {
                console.log(err);
                return res.status(500).json('server error');
            }

            res.send(result);
        })

    } catch (err) {
        console.log(err.message);
        res.status(500).json('server error');
    }
});

module.exports = router;