import React from 'react'
import Navbar from '../../components/UI/NavBar'
import Footer from '../../components/UI/Footer'

function Dashboard() {
    return (
        <>
            <Navbar />
            DASHBOARD
            <Footer />
        </>
    )
}

export default Dashboard
