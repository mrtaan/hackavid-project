import React from 'react'
import Navbar from '../../components/UI/NavBar'
import Footer from '../../components/UI/Footer'
import '../../components/Admin/FormSurvey/Form.css'
import '../../components/Admin/FormSurvey/Form.js'

export default function FormSurvey() {
	return (
		<>
			<Navbar />
			<div className="cardForm--container">
			<div className="container" id="cardForm">
				<div className="row">
					<div className="col-md-6">
						<div className="card card-white">
							<div className="card-body">
								<form action="javascript:void(0);">
									<input type="text" className="form-control add-task" placeholder="New Task..." />
								</form>
								{/* <ul className="nav nav-pills todo-nav">
									<li role="presentation" className="nav-item all-task active"><a href="#" className="nav-link">All</a></li>
									<li role="presentation" className="nav-item active-task"><a href="#" className="nav-link">Active</a></li>
									<li role="presentation" className="nav-item completed-task"><a href="#" className="nav-link">Completed</a></li>
								</ul> */}
								<div className="todo-list">

									<div className="todo-item">
										<button className="btn btn-secondary btn-info btn-inactive mb-3" disabled>Câu 1</button>
										<p>Đà Nẵng bùng dịch</p>
									</div>
									<div className="todo-item">
										<button className="btn btn-secondary btn-info btn-inactive mb-3" disabled>Câu 1</button>
										<p>Đà Nẵng bùng dịch</p>
									</div>
									<div className="todo-item">
										<button className="btn btn-secondary btn-info btn-inactive mb-3" disabled>Câu 1</button>
										<p>Đà Nẵng bùng dịch</p>
									</div>
									<div className="todo-item">
										<button className="btn btn-secondary btn-info btn-inactive mb-3" disabled>Câu 1</button>
										<p>Đà Nẵng bùng dịch</p>
									</div>
									{/* <div className="todo-item">
										<div className="checker"><span className="">Câu 2</span></div>
										<span>Work on wordpress</span>
										<a href="javascript:void(0);" className="float-right remove-todo-item"><i className="icon-close"></i></a>
									</div>

									<div className="todo-item">
										<div className="checker"><span className="">Câu 3</span></div>
										<span>Organize office main department</span>
										<a href="javascript:void(0);" className="float-right remove-todo-item"><i className="icon-close"></i></a>
									</div>
									<div className="todo-item">
										<div className="checker"><span className="">Câu 4</span></div>
										<span>Error solve in HTML template</span>
										<a href="javascript:void(0);" className="float-right remove-todo-item"><i className="icon-close"></i></a>
									</div> */}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			{/* <Footer /> */}
		</>
	)
}