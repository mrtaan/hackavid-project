import React from 'react'
import Navbar from '../../components/UI/NavBar'
import Footer from '../../components/UI/Footer'
import SurveyList from '../../components/Admin/SurveyList';

function Survey() {
    return (
        <>
            <Navbar />
            <SurveyList />
            <Footer />
        </>
    )
}

export default Survey
