import React from 'react';
import ProfileComp from '../../components/User/Profile/Profile';
import Navbar from '../../components/UI/NavBar'
import Footer from '../../components/UI/Footer'


export default function Profile() {
	return(
		<>
		<Navbar />
		<ProfileComp />
		<Footer />
		</>
	);
}