import React from 'react';
import CircleBar from '../../components/User/Circle/CircleBar'
import Chart from '../../components/User/Chart/Chart'
import Tab from '../../components/User/DateTab/Tab'
import SurveyList from '../../components/UI/SurveyList'
import NewList from '../../components/User/News/NewList'
import Navbar from '../../components/UI/NavBar'
import Footer from '../../components/UI/Footer'

function Home() {
    return (
        <>
            <Navbar />
            <CircleBar />
            <Tab />
            <Chart />
            <SurveyList />
            <NewList />
            <Footer />
        </>
    )
}

export default Home;