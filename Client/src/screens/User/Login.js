import React from 'react';
import NavBar from '../../components/UI/NavBar';
import Footer from '../../components/UI/Footer';
import "./Login.css";

export default function Login() {
	return (
		<>
			<NavBar />
			<div className="login--container">
				<div className="container">
					<div className="row">
						<div className="col-md-6">
							<div className="card">
								<form onsubmit="event.preventDefault()" className="box">
									<h1>Đăng nhập</h1>
									<p className="text-muted">Nhập tên đăng nhập và mật khẩu</p> <input type="text" name="" placeholder="Tên đăng Nhập" /> <input type="password" name="" placeholder="Mật khẩu" /> <a className="forgot text-muted" href="#"> Quên mật khẩu?</a> <input type="submit" name="" value="Login" href="#" />
									<div className="col-md-12">
										<ul className="social-network social-circle">
											<li><a href="#" className="icoFacebook" title="Facebook"><i className="fab fa-facebook-f"></i></a></li>
											<li><a href="#" className="icoTwitter" title="Twitter"><i className="fab fa-twitter"></i></a></li>
											<li><a href="#" className="icoGoogle" title="Google +"><i className="fab fa-google-plus"></i></a></li>
										</ul>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Footer />
		</>
	)
}