import React from 'react'
import Navbar from '../../components/UI/NavBar'
import Footer from '../../components/UI/Footer'
function About() {
  return (
    <>
      <Navbar />
      <header className="bg-dark py-5 mb-5">
        <div className="container h-100">
          <div className="row h-100 align-items-center">
            <div className="col-lg-12">
              <p className="lead mb-5 text-white" style={{ textAlign: 'justify' }}>Trong cuộc chiến chống COVID-19 đầy cam go, quyết liệt, việc truy vết, khoanh vùng đối tượng là một vấn đề nhạy cảm và gặp nhiều khó khăn.
              Nhiều ca nhiễm đi đến nơi công cộng gây khó khăn cho việc truy vết, phải nhờ đến truyền thông. Tuy nhiên, có trường các bạn sinh viên đã ở cùng thời gian, địa điểm với người nhiễm bệnh không nắm được thông báo, khả năng dẫn đến rủi ro cao.
              Cùng lúc đó, một số trường đại học khi lấy khai báo của sinh viên về các địa điểm có liên quan tới người lây nhiễm phải thông qua các bạn lớp trưởng tạo form, tổng hợp lại và gửi lên nhà trường, tốn nhiều thời gian, thao tác.
                            <br /><br />Với mong muốn chung tay chống đại dịch, tận dụng thế mạnh về công nghệ thông tin, chúng tôi ấp ủ thiết kế một website dành  riêng cho các cơ quan, tổ chức mà cụ thể là trường học nhằm khắc phục những khó khăn trên, giúp việc khoanh vùng, truy vết đối tượng nhanh chóng, chính xác hơn.<br /><br />Khi có ca lây nhiễm mới, nếu cần thiết quản trị viên sẽ gửi khảo sát đến email tất cả sinh viên, yêu cầu khai báo để nắm tình hình.
                            <br />Website có khả năng tự động hóa trong việc khảo sát thông tin sinh viên chỉ bằng một lần tạo form.<br />
                            Ngoài ra website còn cung cấp thông tin về tình hình dịch bệnh theo từng ngày, tuần và tháng, hỗ trợ tuyên truyền và hướng dẫn nâng cao ý thức của sinh viên về việc phòng chống covid-19.
                            <br /><br />
                            Thân ái.</p>
            </div>
          </div>
        </div>
      </header>
      <div className="container">
        <h1 className="text-center mt-3 mb-5">Nhóm Phát Triển</h1>
        <div className="row">
          <div className="col-md-3 mb-5">
            <div className="card h-69" style={{ border: "none" }}>
              <img className="card-img-top" height='249' src="/assets/images/members/man.png" alt="Anh Diep" with="100%" />
              <div className="card-body">
                <h4 className="card-title text-center">Chu Minh Tân</h4>
              </div>
            </div>
          </div>
          <div className="col-md-3 mb-5">
            <div className="card h-69" style={{ border: "none" }}>
              <img className="card-img-top" height='249' src="/assets/images/members/man.png" alt="Anh Diep" with="100%" />
              <div className="card-body">
                <h4 className="card-title text-center">Đỗ Đông Chiến</h4>
              </div>
            </div>
          </div>
          <div className="col-md-3 mb-5">
            <div className="card h-69" style={{ border: "none" }}>
              <img className="card-img-top" height='249' src="/assets/images/members/man.png" alt="Anh Diep" with="100%" />
              <div className="card-body">
                <h4 className="card-title text-center">Nguyễn Đăng Khoa</h4>
              </div>
            </div>
          </div>
          <div className="col-md-3 mb-5">
            <div className="card h-69" style={{ border: "none" }}>
              <img className="card-img-top" height='249' src="/assets/images/members/woman.png" alt="Anh Diep" with="100%" />
              <div className="card-body">
                <h4 className="card-title text-center">Trần Thị Diệp</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default About;