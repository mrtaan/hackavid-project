import React from 'react'
import './Circle.css'


function Circle({ bgColor, title, number }) {

    return (
        <div className="cirle-card">
            <div className="circle" style={{
                backgroundColor: bgColor,
            }}
            >
                <p className="text">{number}</p>
            </div>
            <h4 style={
                {
                    textAlign: 'center'
                }}>
                {title}
            </h4>
        </div>
    );
}

export default Circle;