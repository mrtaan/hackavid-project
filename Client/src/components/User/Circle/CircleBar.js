import React from 'react'
import Circle from './Circle'
import './CircleBar.css'

function CircleBar() {

    return (
        <div className="container">
            <div className='title'>
                <h3>
                    CA NHIỄM
                    <i className="fas fa-exclamation"></i>
                </h3>
            </div>
            <div className="circle-bar">

                <Circle bgColor="#C4C4C4" title="Hôm nay" number="5" />
                <Circle bgColor="#C4C4C4" title="Tuần này" number="10" />
                <Circle bgColor="#C4C4C4" title="Tháng này" number="18" />
            </div>
        </div>
    )
}

export default CircleBar;