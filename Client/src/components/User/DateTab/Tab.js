import React from 'react'
import './Tab.css'
import TablePerson from '../Patients/Patients'

function Tab() {
    return (
        <div className="tab-contain container">
            <div className='title'>
                <h3>CHI TIẾT
                <i className="fas fa-street-view"></i>
                </h3>
            </div>

            <ul className="nav nav-pills mb-3 title" id="pills-tab" role="tablist">
                <li className="nav-item">
                    <a className="nav-link active day" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" >Hôm Nay</a>
                </li>
                <li className="nav-item ml-2 mr-2">
                    <a className="nav-link" id="pills-week-tab week" data-toggle="pill" href="#pills-week" role="tab" aria-controls="pills-week" aria-selected="false">Tuần Này</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="pills-contact-tab month" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Tháng Này</a>
                </li>
            </ul>
            <div className="tab-content" id="pills-tabContent">
                <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <TablePerson number="5" />
                </div>
                <div className="tab-pane fade" id="pills-week" role="tabpanel" aria-labelledby="pills-week-tab">
                    <TablePerson number="10" />
                </div>
                <div className="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <TablePerson number="18" />
                </div>
            </div>
        </div>
    );
}

export default Tab;
