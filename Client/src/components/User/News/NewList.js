import React from 'react'

import './NewList.css'

function NewList() {
    return (
        <div className="container">
            <div className='title'>
                <h3>
                    TIN TỨC
                    <i className="fas fa-award"></i>
                </h3>
            </div>
            <div className="row d-flex justify-content-between">
                <div className="col-md-6">
                    <img src='/assets/images/news/deo-khau-trang-dung-cach.jpg' style={{ width: '100%' }} alt="" />
                </div>
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header card-header-text">
                            Hướng Dẫn Đeo Khẩu Trang Đúng Cách
                    </div>
                        <div className="card-body">
                            <p className="card-text">Tổ chức Y tế Thế giới (WHO) cảnh báo có 2 con đường cơ bản lây nhiễm của virus corona chủng mới (SARS-CoV-2), một là lây truyền trực tiếp qua việc tiếp xúc không bảo vệ với giọt tiết mũi họng từ người ho, hắt hơi, sổ mũi vào đường hô hấp; hai là lây nhiễm gián tiếp qua bề mặt trung gian đã nhiễm virus.

                            Vì thế, đối với người chưa bị bệnh, việc đeo khẩu trang là biện pháp ngăn chặn giọt bắn chứa virus xâm nhập vào đường hô hấp của chính mình. Còn với người đang trong giai đoạn ủ bệnh, đeo khẩu trang có tác dụng cản trở phát tán của virus ra ngoài. Trong trường hợp này, nếu không đeo khẩu trang, giọt tiết mũi họng chứa virus có thể bắn xa 2m, rất nguy hiểm cho những người xung quanh.

“Đeo khẩu trang sai cách không chỉ tăng nguy cơ lây bệnh mà còn lãng phí tiền bạc. Bởi nếu một người mỗi ngày trung bình sử dụng 3 khẩu trang và với dân số 10 triệu dân như hiện nay thì một ngày tiêu thụ đến 30 triệu khẩu trang mà chưa chắc hiệu quả, rõ ràng quá lãng phí”, bác sĩ Đinh Hải Yến – Trung tâm kiểm soát bệnh tật (Sở Y tế TP.HCM) lưu ý.</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr></hr>

            <div className="row d-flex justify-content-between">
                <div className="col-md-6">
                    <img src='/assets/images/news/9-bien-phap.jpg' style={{ width: '100%' }} alt="" />
                </div>
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header card-header-text">
                            9 biện pháp mới nhất phòng chống dịch COVID-19 người dân cần biết
                    </div>
                        <div className="card-body">
                            <p className="card-text">1. Thường xuyên rửa tay đúng cách bằng xà phòng dưới vòi nước sạch, hoặc bằng dung dịch sát khuẩn có cồn (ít nhất 60% cồn).<br />

2. Đeo khẩu trang nơi công cộng, trên phương tiện giao thông công cộng và đến cơ sở y tế.<br />

3. Tránh đưa tay lên mắt, mũi, miệng. Che miệng và mũi khi ho hoặc hắt hơi bằng khăn giấy, khăn vải, khuỷu tay áo.<br />

4. Tăng cường vận động, rèn luyện thể lực, dinh dưỡng hợp lý xây dựng lối sống lành mạnh.<br />

5. Vệ sinh thông thoáng nhà cửa, lau rửa các bề mặt hay tiếp xúc.<br />

6. Nếu bạn có dấu hiệu sốt, ho, hắt hơi, và khó thở, hãy tự cách ly tại nhà, đeo khẩu trang và gọi cho cơ sở y tế gần nhất để được tư vấn, khám và điều trị.<br />

7. Tự cách ly, theo dõi sức khỏe, khai báo y tế đầy đủ nếu trở về từ vùng dịch.<br />

8. Thực hiện khai báo y tế trực tuyến tại https://tokhaiyte.vn hoặc tải ứng dụng NCOVI từ địa chỉ https://ncovi.vn và thường xuyên cập nhật tình trạng sức khoẻ của bản thân.<br />

9. Cài đặt ứng dụng Bluezone để được cảnh báo nguy cơ lây nhiễm COVID-19, giúp bảo vệ bản thân và gia đình: https://www.bluezone.gov.vn/.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewList;