import React from 'react'
import { Line } from 'react-chartjs-2'
import './Chart.css'

const data = {
    labels: ['03/01', '13/01', '20/01', '11/02', '15/02', '24/02', '27/02', '01/03', '05/03', '15/03', '03/04', '13/04', '20/04', '11/04', '15/04', '24/04', '27/04', '01/05', '05/05', '15/05'],
    datasets: [
        {
            label: 'Ca nhiễm',
            data: [12, 19, 3, 5, 2, 3, 7, 8, 9, 10, 12, 19, 3, 5, 2, 3, 7, 8, 9, 10],
            fill: false,
            backgroundColor: '#7affed',
            borderColor: '#1e2833',
        },
    ],
};

const options = {
    scales: {
        yAxes: [
            {
                ticks: {
                    beginAtZero: true,
                },
            },
        ],
    },
};

function Chart() {

    return (
        <div className="container">
            <div className='title'>
                <h3>20 NGÀY GẦN NHẤT XUẤT HIỆN CA NHIỄM
                <i className="fas fa-chart-line"></i>
                </h3>
            </div>
            <div className="chart-card container">
                <div className='links'>
                </div>
                <Line data={data} options={options} height={"100"} />
            </div>
        </div>
    );
}

export default Chart;
