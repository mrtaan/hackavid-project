import React from 'react'
import './Patients.css'

const data = [
    { id: 1, order: 100, date: '12/04/2019', age: 30, address: 'TPHCM' },
    { id: 2, order: 101, date: '12/04/2019', age: 31, address: 'HN' },
    { id: 3, order: 102, date: '12/04/2019', age: 32, address: 'DN' },
    { id: 4, order: 103, date: '12/04/2019', age: 33, address: 'TPHCM' },
    { id: 5, order: 104, date: '12/04/2019', age: 34, address: 'TPHCM' },
    { id: 6, order: 105, date: '12/04/2019', age: 35, address: 'TPHCM' },
    { id: 7, order: 106, date: '12/04/2019', age: 36, address: 'TPHCM' },
    { id: 8, order: 107, date: '12/04/2019', age: 37, address: 'TPHCM' },
    { id: 9, order: 108, date: '12/04/2019', age: 38, address: 'TPHCM' },
    { id: 10, order: 109, date: '12/04/2019', age: 39, address: 'TPHCM' },
    { id: 11, order: 110, date: '12/04/2019', age: 40, address: 'TPHCM' },
    { id: 12, order: 111, date: '12/04/2019', age: 41, address: 'TPHCM' },
    { id: 13, order: 112, date: '12/04/2019', age: 42, address: 'TPHCM' },
    { id: 14, order: 113, date: '12/04/2019', age: 43, address: 'TPHCM' },
    { id: 15, order: 114, date: '12/04/2019', age: 44, address: 'TPHCM' },
    { id: 16, order: 115, date: '12/04/2019', age: 45, address: 'TPHCM' },
    { id: 17, order: 116, date: '12/04/2019', age: 46, address: 'TPHCM' },
    { id: 18, order: 117, date: '12/04/2019', age: 47, address: 'TPHCM' },
    { id: 19, order: 118, date: '12/04/2019', age: 48, address: 'TPHCM' },

]


function Patients(props) {

    const exportData = () => {
        let arr = []
        for (let i = 0; i < props.number; i++) {
            arr.push(data[i]);
        }
        return arr;
    }

    return (
        <div className="table-wrapper-scroll-y my-custom-scrollbar">
            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Bệnh Nhân Số</th>
                        <th scope="col">Ngày Phát Hiện</th>
                        <th scope="col">Tuổi</th>
                        <th scope="col">Địa Chỉ</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        exportData().map((object, index) => {
                            return (
                                <tr key={index}>
                                    <th scope="row">{object.id}</th>
                                    <td>{object.order}</td>
                                    <td>{object.date}</td>
                                    <td>{object.age}</td>
                                    <td>{object.address}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Patients;