import React, { useState, useEffect } from 'react';
import './Profile.css';
import Axios from 'axios';
import {Link} from 'react-router-dom'
export default function Profile() {

	const [surveyList, setSurveyList] = useState([]);

	useEffect(async () => {
		Axios.get(
			'http://localhost:5000/api/survey/list',
		).then(response => {
			const data = response.data;
			setSurveyList(data.surveys)
		})
	});
	return (
		<>
			<div className="row py-5 px-4 profile--container">
				<div className="col-md-9 mx-auto">
					{/* <!-- Profile widget --> */}
					<div className="bg-white shadow rounded overflow-hidden">
						<div className="px-4 pt-0 pb-4 cover">
							<div className="media align-items-end profile-head">
								<div className="profile mr-3">
									<img src="./assets/images/profile/annonymous_av.png" alt="avatar" width="130" className="rounded mb-2 img-thumbnail" /><a href="#" className="btn btn-outline-dark btn-sm btn-block">Edit profile</a>
								</div>
								<div className="media-body mb-5 text-white">
									<h4 className="mt-0 mb-0">Đỗ Đông Chiến</h4>
									<p className="small mb-4"> <i className="fas fa-map-marker-alt mr-2"></i>Khánh Hoà</p>
								</div>
							</div>
						</div>
						<div className="bg-light p-4 d-flex text-center">
							<img src="" className="mr-3" width="130" />
							<h2 className="mt-0 mb-0">Đại học Công nghệ Thông tin</h2>
						</div>
						<div className="px-4 py-3">
							<h5 className="mb-0">About</h5>
							<div className="p-4 rounded shadow-sm bg-light">
								<p className="font-italic mb-0">Web Developer</p>
								<p className="font-italic mb-0">Lives in New York</p>
								<p className="font-italic mb-0">Photographer</p>
							</div>
						</div>
						<table className="table">
							<thead>
								<tr>
									<th scope="col" className="text-center">#</th>
									<th scope="col" className="text-center">Tiêu đề</th>
									<th scope="col" className="text-center">BN Số</th>
									<th scope="col" className="text-center">Thao Tác</th>
								</tr>
							</thead>
							<tbody>
								{
									surveyList.map((survey, index) => {
										return (
											<tr>
												<th scope="row"> {survey.SURVEY_ID} </th>
												<td>{survey.SURVEY_NAME}</td>
												<td>{survey.PATIENT_ID}</td>
												<td className="text-center">
													<a href={"http://localhost:5000/api/survey/answer/" + survey.SURVEY_ID }> Làm Khảo Sát</a>
												</td>
											</tr>
										)
									})
								}


							</tbody>
						</table>
					</div>
				</div>
			</div>

		</>

	)
}