import React from 'react'
import { Link } from 'react-router-dom';

export default function NavBar() {
    return (
        <nav className="navbar navbar-expand-md navbar-dark" style={{ backgroundColor: '#000' }}>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse order-2" id="navbarSupportedContent" style={{ justifyContent: 'space-around' }}>
                <Link className="navbar-brand" to="/">
                    <div className="d-flex align-items-center">
                        <img src="/logo_red.png" width="80" className="d-inline-block align-top" alt="" />
                        <span style={{ color: '#15cdfc' }}>Hackavid</span>
                    </div>
                </Link>

                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/">Trang Chủ<span className="sr-only">(current)</span></Link>
                    </li>

                    <li className="nav-item">
                        <Link className="nav-link" to="/about">Giới Thiệu</Link>
                    </li>

                </ul>

                <ul className="navbar-nav">
                    <li className="">
                        <Link className="nav-link" to="/login">
                            <div className="d-flex align-items-center">
                                <i className="fas fa-user-circle fa-2x"></i>
                                <span className="ml-3">Đăng Nhập</span>
                            </div>
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}
