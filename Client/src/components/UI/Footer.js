import React from 'react'

function Footer() {

    return (
        <footer className="bg-dark text-center text-white container-fluid pb-2 mt-2">
            <div className="row d-flex justify-content-between align-items-center">
                <div className="col-md-5">
                    <img src='/logo_blue.png' width="50%" alt="" />
                </div>
                <div className="col-md-7">
                    <h4 style={{ borderBottom: '4px solid #2dfce1', display: 'inline-block' }} className="mb-4">
                        <strong>HACKAVID</strong> VÌ MỘT THẾ HỆ SINH VIÊN KHỎE MẠNH
                    </h4>
                    <p><i className="fas fa-phone-square-alt text-right mr-2"></i> Điện thoại: 039 248 9615</p>
                    <p><i className="fas fa-map-marker-alt mr-2" ></i> Địa chỉ: Trường Đại học Công nghệ Thông tin ĐHQG ,Khu phố 6, Thủ Đức, TP. Hồ Chí Minh</p>
                </div>
            </div>
            <div className="row d-flex justify-content-between align-items-enter">
                <em className="col-md-12" style={{ fontSize: '12px' }}>@2021 POWERED BY TEAM32 WEBDEV-HACKATHON</em>
            </div>
        </footer>

    );
}

export default Footer;