import React from 'react'

function SurveyList() {

    return (
        <div className="survey-list container">
            <div className='title'>
                <h3>
                    BẢNG KHẢO SÁT
                    <i className="far fa-list-alt"></i>
                </h3>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Khảo Sát</th>
                        <th scope="col">Bệnh Nhân Số</th>
                        <th scope="col">Link</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Quận 11</td>
                        <td>BN1003</td>
                        <td><a href="/survey">Làm Khảo Sát</a></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Địa Điểm Liên Quan Đến Ca Nhiễm</td>
                        <td>BN1009</td>
                        <td><a href="/survey">Làm Khảo Sát</a></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Địa Điểm Liên Quan Đến Ca Nhiễm</td>
                        <td>BN1823</td>
                        <td><a href="/survey">Làm Khảo Sát</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default SurveyList;