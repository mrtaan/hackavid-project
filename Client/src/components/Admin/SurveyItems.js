export const SurveyItems = [
	{
		title:'Đà Nẵng bùng dịch',
		patientName: 'Đỗ Đông Chiến',
		date: '12/03/2001',
		place: 'Quận Thủ Đức',
		patientNum: '1234',
		urlName: 'Sân bay Vân Đồn đóng cửa',
		url: 'https://tuoitre.vn/tiep-tuc-dong-cua-san-bay-van-don-den-ngay-3-3-20210222103712759.htm',
		cName: 'nav-links'
	},
	{
		title:'Sân bay Cam Ranh',
		patientName: 'Trần Thị Diệp',
		date: '11/11/2020',
		place: 'Quận 10',
		patientNum: '1023',
		urlName: 'Sân bay Cam Ranh thất thủ',
		url: 'https://camranh.aero/vi/tin-tuc',
		cName: 'nav-links'
	},
	{
		title:'Vượt biên Campuchia',
		patientName: 'Nguyễn Khoa Đăng',
		date: '28/4/2021',
		place: 'Quận 12',
		patientNum: '2345',
		urlName: 'Nhập cảnh trái phép báo nhân dân',
		url: 'https://camranh.aero/vi/tin-tuc',
		cName: 'nav-links'
	},
	{
		title:'Hà Nội phong toả',
		patientName: 'Chu Minh Tân',
		date: '1/5/2021',
		place: 'Quận 12',
		patientNum: '2222',
		urlName: 'Hàng loạt người TQ nhập cảnh biên giới phía Bắc',
		url: 'https://tuoitre.vn/chi-trong-4-gio-ngay-26-4-phat-hien-31-nguoi-nhap-canh-trai-phep-20210426160600687.htm',
		cName: 'nav-links'
	},
	{
		title:'Du học sinh Ấn Độ',
		patientName: 'Nguyễn Văn A',
		date: '29/4/2021',
		place: 'Kiên Giang',
		patientNum: '2865',
		urlName: 'Ấn Độ ghi nhận ca nhiễm kỷ lục',
		url: 'http://thoibaotaichinhvietnam.vn/pages/quoc-te/2021-04-28/covid-19-toi-sang-28-4-an-do-vuot-200000-ca-tu-vong-ca-nhiem-thuc-co-the-toi-hon-nua-ty-nguoi-103111.aspx',
		cName: 'nav-links'
	},
]
