import React, { useEffect, useState } from 'react';
import { SurveyItems } from './SurveyItems'
import Axios from 'axios';


export default function SurveyList() {
	const [surveyList, setSurveyList] = useState([]);
	const [studentCount, setStudentCount] = useState(0);

	useEffect(async () => {
		Axios.get(
			'http://localhost:5000/api/survey/list',
		).then(response => {
			const data = response.data;
			setSurveyList(data.surveys)
			setStudentCount(data.studentCount)
		})
	});


	return (
		<>
			<div className="survey-list container">
				<div className='title'>
					<h3>
						BẢNG KHẢO SÁT
					<i className="far fa-list-alt"></i>
					</h3>
				</div>
				<div className="container survey--list" style={{ margin: "39px" }}>
					<button type="button" className="btn btn-secondary btn-lg">
						<Link to="/admin/form">Tạo khảo sát</Link>
					</button>
				</div>
				<table className="table table-bordered table-hover text-center">
					<thead className="thead-dark">
						<tr>
							<th scope="col">Số thứ tự</th>
							<th scope="col">Tên Khảo Sát</th>
							<th scope="col">Bệnh nhân liên quan số</th>
							<th scope="col">Ngày tạo</th>
							<th scope="col">Tin chi tiết</th>
						</tr>
					</thead>
					{SurveyItems.map((item, index) => { //Iterate qua SurveyLists.js từng phần tử
						return (
							<>
								<tbody>
									<tr className={index % 2 == 0 ? 'table-info' : 'table-light'}>
										<th scope="row"> {index + 1} </th>
										<td>{item.place}</td>
										<td>{item.patientNum}</td>
										<td>{item.date}</td>
										<td><a href={item.url}>{item.urlName}</a></td>
									</tr>
								</tbody>
							</>
						)
					})}
				</table>
			</div>
			<div className="container survey--list" style={{ margin: "39px" }}>
				<button type="button" className="btn btn-secondary btn-lg">Tạo khảo sát</button>
			</div>
			<table className="table table-bordered table-hover text-center">
				<thead className="thead-dark">
					<tr>
						<th scope="col">Số thứ tự</th>
						<th scope="col">Tiêu đề</th>
						<th scope="col">Bệnh nhân số</th>
						<th scope="col">Tổng SV</th>
						<th scope="col">Đã Làm</th>
						<th scope="col">Chưa Làm</th>
						<th scope="col">Ngày tạo</th>
					</tr>
				</thead>
				{
					surveyList.map((survey, index) => { //Iterate qua SurveyLists.js từng phần tử
						return (
							<tbody>
								<tr className={index % 2 == 0 ? 'table-info' : 'table-light'}>
									<th scope="row"> {survey.SURVEY_ID} </th>
									<td>{survey.SURVEY_NAME}</td>
									<td>{survey.PATIENT_ID}</td>
									<td>{studentCount}</td>
									<td>{survey.student_maked}</td>
									<td>{studentCount - survey.student_maked}</td>
									<td>{survey.TIME}</td>
								</tr>
							</tbody>
						)
					})
				}
			</table>
		</>
	)
}