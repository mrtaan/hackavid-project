import './App.css';
import React, { Fragment } from 'react'

import Footer from './components/UI/Footer'

import About from './screens/User/About'
import Home from './screens/User/Home'
import FormSurvey from './screens/Admin/Form';

import Dashboard from './screens/Admin/Dashboard'
import Survey from './screens/Admin/Survey'
import Profile from './screens/User/Profile';
import Login from './screens/User/Login';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import PageNotFound from './PageNotFound';

function App() {
  return (
    <Router>
      <Fragment>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' exact component={About} />
          <Route path='/profile' exact component={Profile} />
          <Route path='/login' exact component={Login} />
        </Switch>
      </Fragment>

      <Fragment>
        <Switch>
          <Route path='/admin' exact component={Dashboard} />
          <Route path='/admin/survey' exact component={Survey} />
          <Route path='/admin/form' exact component={FormSurvey} />
        </Switch>
      </Fragment>
    </Router>
  );

}

export default App;
