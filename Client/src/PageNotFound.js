import React from 'react'

function PageNotFound() {


    return (
        <div className="container mt-5">
            <div className="row d-flex justify-content-between" >
                <div className="col-md-12"></div>
                <h1>404 Trang Không Tồn Tại</h1>
            </div>
            <div className="row d-flex justify-content-between" >
                <div className="col-md-12">
                    <a className="btn btn-success" href={'/'}>Quay Về Trang Chủ</a>
                </div>
            </div>

        </div>
    );
}

export default PageNotFound;